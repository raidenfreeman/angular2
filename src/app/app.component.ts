import {Component} from "@angular/core";
import {Prescription} from "./classes/prescription";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

/*export class PrescriptionComponent {
  model = new Prescription(0, '', '', '', 0, false, 0, 0);
  submitted = false;

  onSubmit() {
    this.submitted = true;
  }

  active = true;

  newPrescription() {
    this.model = new Prescription(0, '', '', '', 0, false, 0, 0);
    this.active = false;
    setTimeout(()=>this.active = true, 0);
  }
}*/

export class AppComponent {
  clicked(event){
    event.preventDefault();
    console.log("hi");
  }

  prescriptions = [
    {
      customerCode: 100,
      customerSurname: "Gkinis",
      customerName: "Konstantinos",
      barcode: "0254987414",
      iteration: 3,
      paid: true,
      validFrom: new Date('2016-08-12'),
      validTo: new Date('2016-12-20')
    }
    , {
      customerCode: 100,
      customerSurname: "Gkinis",
      customerName: "Konstantinos",
      barcode: "1154987414",
      iteration: 2,
      paid: true,
      validFrom: new Date('2016-01-02'),
      validTo: new Date('2016-12-10')
    },
    {
      customerCode: 101,
      customerSurname: "Gkinis",
      customerName: "Petros",
      barcode: "02523174",
      iteration: 1,
      paid: true,
      validFrom: new Date('2016-05-12'),
      validTo: new Date('2016-12-30')
    }
  ];
}


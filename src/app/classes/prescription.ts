/**
 * Created by Konnos on 10/21/2016.
 */

export class Prescription {
  constructor(public code: number,
              public name: string,
              public surname: string,
              public barcode: string,
              public iteration: number,
              public paid: boolean,
              public validFrom: number,
              public validTo: number) {
  }
}
